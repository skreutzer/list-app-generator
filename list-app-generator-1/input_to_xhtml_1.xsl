<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020 Stephan Kreutzer

This file is part of list-app-generator-1.

list-app-generator-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

list-app-generator-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with list-app-generator-1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:ogmp="htx-scheme-id://com.openglobalmind.20140426T190412Z/projects.20200819T214500Z" exclude-result-prefixes="ogmp">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by input_to_xhtml_1.xsl of list-app-generator-1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/list-app-generator/ and https://groupware-systems.org). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>
          <xsl:text>Projects</xsl:text>
        </title>
      </head>
      <body>
        <xsl:apply-templates select="./ogm-projects/ogmp:ogm-project"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project">
    <div class="entry">
      <xsl:apply-templates select="./ogmp:name"/>
      <xsl:apply-templates select="./ogmp:who"/>
      <xsl:apply-templates select="./ogmp:time-horizon"/>
      <xsl:apply-templates select="./ogmp:purpose"/>
      <xsl:apply-templates select="./ogmp:how-to-join"/>
    </div>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:name">
    <h2>
      <xsl:apply-templates/>
    </h2>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:name//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:who">
    <div>
      <xsl:text>Who: </xsl:text>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:who//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:time-horizon">
    <div>
      <xsl:text>Time horizon: </xsl:text>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:time-horizon//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:purpose">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:purpose//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:how-to-join">
    <div>
      <xsl:text>How to join: </xsl:text>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="/ogm-projects/ogmp:ogm-project/ogmp:how-to-join//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
