#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of list-app-generator-1.
#
# list-app-generator-1 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# list-app-generator-1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with list-app-generator-1. If not, see <http://www.gnu.org/licenses/>.

mkdir -p ./temp/
mkdir -p ./output/

java -cp ./digital_publishing_workflow_tools/file_discovery/file_discovery_1/ file_discovery_1 ./jobfile_file_discovery_1.xml ./resultinfo_file_discovery_1.xml
java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_1.xml ./resultinfo_xml_xslt_transformator_1_1.xml
java -cp ./digital_publishing_workflow_tools/xml_concatenator/xml_concatenator_1/ xml_concatenator_1 ./temp/jobfile_xml_concatenator_1.xml ./resultinfo_xml_concatenator_1.xml
java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_2.xml ./resultinfo_xml_xslt_transformator_1_2.xml
java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_3.xml ./resultinfo_xml_xslt_transformator_1_3.xml
