#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of list-app-generator-1.
#
# list-app-generator-1 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# list-app-generator-1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with list-app-generator-1. If not, see <http://www.gnu.org/licenses/>.

sudo apt-get install default-jre unzip wget

wget https://github.com/OpenGlobalMind/ogm-project-list/archive/master.zip
unzip ./master.zip
mv ./ogm-project-list-master/projects/ ./
rm -rf ./ogm-project-list-master/
rm ./master.zip
