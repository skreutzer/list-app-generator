<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020 Stephan Kreutzer

This file is part of list-app-generator-1.

list-app-generator-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

list-app-generator-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with list-app-generator-1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <xml-concatenator-1-job>
      <input-files>
        <xsl:apply-templates select="./entry-list/entries/entry"/>
      </input-files>
      <output-file path="./input_concatenated.xml" processing-instruction-data="encoding=&quot;UTF-8&quot;" root-element-name="ogm-projects"/>
    </xml-concatenator-1-job>
  </xsl:template>

  <xsl:template match="/entry-list/entries/entry">
    <input-file path="{@path}"/>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
