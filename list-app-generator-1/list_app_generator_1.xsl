<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2020 Stephan Kreutzer

This file is part of list-app-generator-1.

list-app-generator-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

list-app-generator-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with list-app-generator-1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:src="http://www.w3.org/1999/xhtml">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by list_app_generator_1.xsl of list-app-generator-1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/list-app-generator/ and https://groupware-systems.org). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2020 Stephan Kreutzer

This file is part of list-app-generator-1.

list-app-generator-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

list-app-generator-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with list-app-generator-1. If not, see &lt;http://www.gnu.org/licenses/&gt;.

The data in the &lt;div id="entries"/&gt; is not part of this program,
it's user data that is only processed. A different license may apply.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>
          <xsl:text>Open Global Mind: Projects</xsl:text>
        </title>
        <link rel="icon" href="./favicon.png" type="image/png"/>
        <style type="text/css">
          body
          {
              font-family: sans-serif;
              color: #FFFF00;
              background-color: #C0C0C0;
          }
        </style>
        <link rel="manifest">
          <xsl:attribute name="href">data:application/manifest+json,{
  "name": "Open Global Mind: Projects",
  "short_name": "Open Global Mind: Projects",
  "start_url": "projects.xhtml",
  "scope": "projects.xhtml",
  "display": "standalone",
  "background_color": "#000000",
  "description": "List of projects known in the context of Open Global Mind.",
  "icons":
  [
    {
      "src": "launcher-icon-1x.png",
      "sizes": "48x48",
      "type": "image/png"
    },
    {
      "src": "launcher-icon-2x.png",
      "sizes": "96x96",
      "type": "image/png"
    },
    {
      "src": "launcher-icon-4x.png",
      "sizes": "192x192",
      "type": "image/png"
    }
  ],
  "lang": "en",
  "related_applications":
  [
    {
      "platform": "web",
      "url": "https://groupware-systems.org/skreutzer/ogm/projects.xhtml"
    }
  ]
}
          </xsl:attribute>
        </link>
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width"/>
        <script type="text/javascript" src="serviceWorker.js"></script>
        <script type="text/javascript">
<xsl:text>
          "use strict";

          function registerServiceWorker()
          {
              if ("serviceWorker" in navigator)
              {
                  navigator.serviceWorker.register('serviceWorker.js')
                    .then(function(reg) {
                        console.log("Service worker registered.");
                    })
                    .catch(function(err) {
                        console.log(err);
                    });
              }
              else
              {
                  console.log("Could not find serviceWorker in navigator.");
              }
          }

          registerServiceWorker();

          let currentEntry = 0;

          function navigateTo(newEntry)
          {
              let entries = document.getElementsByClassName('entry');
              let max = entries.length;

              if (max &lt;= 0)
              {
                  return 1;
              }

              if (newEntry &lt; 0 || newEntry &gt;= max)
              {
                  return 1;
              }

              for (let i = 0; i &lt; max; i++)
              {
                  entries[i].style.display = "none";
              }

              currentEntry = newEntry;
              entries[currentEntry].style.display = "inline";

              return 0;
          }

          window.onload = function() {
              navigateTo(0);
          };
</xsl:text>
        </script>
      </head>
      <body>
        <div id="navigation">
          <a href="#" onclick="navigateTo(currentEntry - 1);">prev</a>
          <xsl:text> </xsl:text>
          <a href="#" onclick="navigateTo(currentEntry + 1);">next</a>
        </div>
        <div class="entries">
          <xsl:comment> The data contained in this element and sub-elements is not part of this program. It's user data and might be under a different license than this program. This program also doesn't depend on it or link it as a library, it's only processed. </xsl:comment>
          <xsl:apply-templates select="/src:html/src:body/src:div[@class='entry']"/>
          <sdf/>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/src:html/src:body/src:div[@class='entry']">
    <a/>
    <xsl:copy>
      <xsl:apply-templates select="@*|node()|text()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/src:html/src:body/src:div[@class='entry']//node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()|text()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/src:html/src:body/src:div[@class='entry']//@*">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="/src:html/src:body/src:div[@class='entry']//text()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
