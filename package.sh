#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of list-app-generator.
#
# list-app-generator is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# list-app-generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with list-app-generator. If not, see <http://www.gnu.org/licenses/>.

sudo apt-get install wget zip unzip make default-jdk

rm -r ./packages/list-app-generator-1/
mkdir -p ./packages/list-app-generator-1/
cp -r ./list-app-generator-1/ ./packages/

cd ./packages/
cd ./list-app-generator-1/

printf "Build date: $(date "+%Y-%m-%d").\n" > version.txt
currentDate=$(date "+%Y%m%d")

cd ..
cd ..


cd ./list-app-generator-1/
wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip
unzip digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
cd workflows
cd setup
cd setup_1
java setup_1
cd ..
cd ..
cd ..
cd ..
cd ..


mkdir -p ./packages/list-app-generator-1/digital_publishing_workflow_tools/file_discovery/file_discovery_1/
cp -r ./list-app-generator-1/digital_publishing_workflow_tools/file_discovery/file_discovery_1/ ./packages/list-app-generator-1/digital_publishing_workflow_tools/file_discovery/

mkdir -p ./packages/list-app-generator-1/digital_publishing_workflow_tools/xml_concatenator/xml_concatenator_1/
cp -r ./list-app-generator-1/digital_publishing_workflow_tools/xml_concatenator/xml_concatenator_1/ ./packages/list-app-generator-1/digital_publishing_workflow_tools/xml_concatenator/

mkdir -p ./packages/list-app-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./list-app-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ ./packages/list-app-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/


cd ./packages/

zip -r ./list-app-generator-1_$currentDate.zip list-app-generator-1
sha256sum ./list-app-generator-1_$currentDate.zip > ./list-app-generator-1_$currentDate.zip.sha256
rm -r ./list-app-generator-1/

cd ..
